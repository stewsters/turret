#include <Servo.h>

Servo yservo;  
Servo xservo; 

int ypos = 0;
int xpos= 0;

void setup(){
  xservo.attach(10); 
  yservo.attach(11); 
 
  Serial.begin(19200); // 19200 is the rate of communication
  Serial.println("Running"); // some output for debug purposes.
  
  pinMode(13,OUTPUT);
 
}

void loop() {
  static int v = 0; // value to be sent to the servo (0-180)
  if ( Serial.available()) {
    char ch = Serial.read(); // read in a character from the serial port and assign to ch
    switch(ch) { // switch based on the value of ch
      case '0'...'9': // if it's numeric
        v = v * 10 + ch - '0';
        break;
      case 'x':
        xservo.write(v);
        v = 0;
        break;
      case 'y':
        yservo.write(v);
        v = 0;
        break;
      case 'h':
        digitalWrite(13, HIGH);
        break; 
      case 'l':
        digitalWrite(13, LOW);
        break;  
    }
  }
}
